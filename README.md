# Rommie [![Build Status](http://jenkins.k-4u.nl/buildStatus/icon?job=Rommie)](http://jenkins.k-4u.nl/me/my-views/view/All/job/Rommie/)

Level 3 Commands
================
###Beam
**This command is still under development.** <br>
In the current state it will go fetch the status of Tlovetech's beam stream.

###Boop	 
Boops a supplied target, if no target is supplied everyone will get booped.

###Bug
Do you have a bug or an idea for more functionality? <br>
Use this command to get a link to the Rommie issue tracker on GitHub.

###Flail
Flails at supplied target, if no target is supplied everyone will get flailed at.

###Fox	
Throws a fox at a supplied target, if no target is supplied everyone will get foxes thrown at them.

###FoxImage
Get a random fox image from the community wallpaper project (http://sourcecoded.info/wallpaper/).

###Google
Allows you to pass a query to Google and receive the first result.

###Help
Supply a command to this command for information on it. <br>
Specifying no command returns a full list of registered commands for all levels.

###Hug
Hugs a supplied target, if no target is supplied everyone will get a hug.

###Penguin	
This command was created for joshiejack. <br>
Turns a supplied target into a penguin, without a target the penguins shall kill you all.

###Prefix
State the current commnand prefix.

###Quote
Get a random quote form www.iheartquotes.com using the quote module.

###Riot
**This command is currently not working as expected.**

###Source
Returns a link to the Rommie GitHub. 

###Table
**This command is currently not working as expected.**

###TimeoutLink	
**This is a channel specific command and can not be used elsewhere**

###Tribble	
This command was created for ApSciLiara. <br>
Pelts a specified user with tribbles, if no target is specified you better hold your breath!

###UnTable	
**This command is currently not working as expected.** 

###UploadFox
Uploads an image to the community wallpaper project (http://sourcecoded.info/wallpaper/). <br>
These images are returned as the random foxes and can be called on demand using the command prefix with 'foximage' appended.


Level 2 Commands
================
###Timeout
**This is a channel specific command and can not be used elsewhere**

Level 1 Commands
================
###Part	
This tell Rommie to leave a channel.

###SetTopic
If Rommie has appropriate permissions this command can be used to set the topic of a channel.


Level 0 Commands
================
###Join	
Tells Rommie to join a specified channel.

###ReloadCommands
**This command is no longer in use and is included for historical reasons**

###SetPrefix
Allows for the command prefix to be changed from the default '>'.

###ToggleFox
Toggle the Random foxes that are triggered by the use of the word 'fox'.

###ToggleGreeting	
Toggle general greetings when a user joins a channel.<br>
Personalised greetings are not effected.

###TogglePotato	 
Toggle the potato message that is randomly triggered on the word 'potato'.

###TogglePrefix
Toggle the choice of Rommie stating her current command prefix when joining a channel.
