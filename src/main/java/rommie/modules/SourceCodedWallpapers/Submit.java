package rommie.modules.SourceCodedWallpapers;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Submit {

    public static String Submit(String message, String[] arguments) {
        int starting_point = message.indexOf("uploadfox") + "uploadfox ".length();

        String message_to_send = message.substring(starting_point).trim();

        HttpURLConnection connection = null;
        try {
            URL url = new URL("http://sourcecoded.info/wallpaper/submitGet.php?title=Fox&source=Rommie&url=" + message_to_send + "&nsfw=false&tags=fox");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "BOT_ROMMIE");
            connection.connect();

            switch (connection.getResponseCode()) {
                case 200:
                    return "Fox image uploaded!";
                default:
                    return String.format("Could not upload: %s [%s]", connection.getResponseCode(), connection.getResponseMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return String.format("Could not upload: %s", e.getCause());
        }
    }

}
