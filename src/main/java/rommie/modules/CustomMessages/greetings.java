package rommie.modules.CustomMessages;

import rommie.Rommie;

import static rommie.modules.RandomNumber.RandomNumber.generateRandom;

public class greetings {

    public static void check(Rommie instance, String sender, String channel){

        //Just for MoxieGrrl since she likes the greetings
        if(sender.equalsIgnoreCase("MoxieGrrl") && channel.equalsIgnoreCase("#Kihira")) {
            instance.sendAction(channel, instance.Moxie[generateRandom(instance.Moxie.length)]);
        }

        //--------------------------------------------------------------------------------------------------------------

        //Just for Othlon
        if(sender.equalsIgnoreCase("Othlon") && channel.equalsIgnoreCase("#Kihira")) {
            instance.sendAction(channel, "pounces on Othlon");
        }

        //--------------------------------------------------------------------------------------------------------------

        //Just for AtomicBlom
        if(sender.equalsIgnoreCase("AtomicBlom") && channel.equalsIgnoreCase("#Kihira")) {
            instance.sendAction(channel, "snobs off AtomicBlom");
        }

    }
}
