package rommie.modules.CustomMessages;

import org.json.simple.parser.ParseException;
import rommie.Rommie;
import rommie.modules.SourceCodedWallpapers.GetWallpaper;

import static rommie.modules.RandomNumber.RandomNumber.generateRandom;

public class responces {

    public static void generalMessage(String channel, String sender, String message, Rommie instance) throws ParseException, InterruptedException {

        //Quack like a duck
        if (message.contains("quack") | message.contains("Quack") && !message.contains(instance.CMD_PREFIX) && generateRandom(5) == 5) {
            instance.sendMessage(channel, "Quack, Quack.");
            instance.sendMessage(channel, "I'm a duck!");
        }

        //--------------------------------------------------------------------------------------------------------------

        //Random fox image
        if (message.contains("fox") && !message.contains(instance.CMD_PREFIX) && generateRandom(25) == 5) {
            instance.sendMessage(channel, GetWallpaper.webpage());
        }

        //--------------------------------------------------------------------------------------------------------------

        //Random fox when kihira says fox (100% of the time)
        //Just for Kihira, even I don't have one of these!
        if(sender.equalsIgnoreCase("kihira") && message.contains("fox") && !message.contains(instance.CMD_PREFIX)){
            GetWallpaper.getWallpaper();
            instance.sendMessage(channel, GetWallpaper.webpage());
        }

        //--------------------------------------------------------------------------------------------------------------

        //I'm a potato!
        if (message.contains("potato")  && !message.contains(instance.CMD_PREFIX) && generateRandom(10) == 5){
            instance.sendMessage(channel, "I'M A POTATO!");
        }

        //--------------------------------------------------------------------------------------------------------------

        //I'm a boob!
        if (message.contains("boobs")  && !message.contains(instance.CMD_PREFIX) && generateRandom(10) == 5){
            instance.sendAction(channel, "gives " + sender  + " boobs.");
        }

        //--------------------------------------------------------------------------------------------------------------

        //I'm a penguin!
        //Just for Joshie, really need to get one of these
        if (message.contains("joshie") && generateRandom(32) == 3) {
            instance.sendMessage(channel, "Noot Noot!");
        }

        //--------------------------------------------------------------------------------------------------------------

        //LONG LIVE THE FOXTRESS
        if (message.contains("foxtress")){
            instance.sendMessage(channel, "LONG LIVE FOXTRESS KIHIRA!");
        }
    }
}
