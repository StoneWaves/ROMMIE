package rommie.modules.HighFive;

import rommie.Rommie;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HighFives {

    static Pattern lead = Pattern.compile("^.*(^|\\s)(o/+|o\\\\+)($|\\s).*$");
    static Pattern end = Pattern.compile("^.*(^|\\s)(\\\\+o|/+o)($|\\s).*$");

    static String lastLead;
    static String lastLeadNick;

    static File highFives = new File("Rommie.highFives");

    static BufferedReader reader;
    static ArrayList<String> s;
    static long timeout = 60 * 1000;
    static long timeSet;

    public static void doIt(String message, String user, String nick, String channel, Rommie rommie) {
        Matcher lMatch = lead.matcher(message);
        Matcher eMatch = end.matcher(message);

        if (lMatch.matches()) {
            lastLead = user;
            lastLeadNick = nick;
            timeSet = System.currentTimeMillis();
        } else if (eMatch.matches() && lastLead != null && !lastLead.equals("") && !lastLead.equals(user)) {
            if (System.currentTimeMillis() - timeSet > timeout) {
                lastLead = null;
                return;
            }
            String starter = lastLead; lastLead = null;
            String count = "1";

            try {
                //FORMAT: USER:USER:COUNT
                if (!highFives.exists()) highFives.createNewFile();
                if (reader == null) {
                    initReader();
                }
                int index = findLine(user, starter);
                if (index == -1) {
                    s.add(user + ":" + starter + ":" + 1);
                } else {
                    String l = s.get(index);
                    String[] split = l.split(":");
                    int i = Integer.parseInt(split[2]) + 1;
                    count = String.valueOf(i);
                    s.set(index, user + ":" + starter + ":" + i);
                }
                save();

            } catch (Exception e) {e.printStackTrace();}

            String reply = String.format("%s o/ * \\o %s - %s time", lastLeadNick, nick, count + (count.endsWith("1") ? "st" : count.endsWith("2") ? "nd" : count.endsWith("3") ? "rd" : "th"));
            System.out.println(reply);
            rommie.sendMessage(channel, reply);
        }
    }

    static void initReader() throws IOException {
        reader = new BufferedReader(new FileReader(highFives));
        s = new ArrayList<String>();
        String last;
        while ((last = reader.readLine()) != null)
            s.add(last);
        reader.close();
    }

    static int findLine(String user, String user2) {
        for (int i = 0; i < s.size(); i++) {
            String line = s.get(i);
            String[] split = line.split(":");
            if ((split[0].equals(user) && split[1].equals(user2)) || (split[0].equals(user2) && split[1].equals(user))) {
                return i;
            }
        }
        return -1;
    }

    static void save() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(highFives));
        for (String line : s) {
            writer.write(line+"\n");
        }
        writer.close();
    }

}
