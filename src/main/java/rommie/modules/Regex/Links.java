package rommie.modules.Regex;

import rommie.Rommie;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Links {

   /*

   Regex from saucebot
    /(?:(?:(?:https?:\/\/[-a-zA-Z0-9\.]*)|(?:[-a-zA-Z0-9]+\.))[-a-zA-Z-0-9]+\.(?:[a-zA-Z]{2,})\b|(?:\w+\.(?:[a-zA-Z]{2,}\/|(com|net|org|ru))))/

    My regex
    (?i)\(?\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]
     */

    //Pull all links from the body for easy retrieval
    public static ArrayList pullLinks(String text, Rommie instance) {
        ArrayList links = new ArrayList();

        String regex = "(?i)\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        System.out.println("To match : " + text);
        while(m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")"))
            {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            if(urlStr.contains("youtube.com/watch")){
                links.add(urlStr);
                System.out.println("Extracted URL : " + urlStr);
                //instance.sendMessage("#Rommie", urlStr.toLowerCase());
            }

        }
        return links;
    }


}
