package rommie.modules.Regex;

import rommie.Rommie;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * pls
 *
 * @author Jaci
 */
public class RegexReplace {

    public static class Message {
        public String message;
        public String sender;
    }

    public static final Pattern shouldReplace = Pattern.compile("^([sm])/(.*?(?<!\\\\))/(?:(.*?(?<!\\\\))/)?(.*)");

    static HashMap<String, EvictingQueue<Message>> history = new HashMap<>();

    public static void tryReplace(String channel, String message, String sender, Rommie rommie) {
        Matcher match = shouldReplace.matcher(message);
        if (!history.containsKey(channel)) history.put(channel, new EvictingQueue<Message>(40));

        if (match.matches()) {
            String pattern = match.group(2);
            String replace = match.group(3);
            String args = match.group(4);
            boolean singleRun = true;

            if (pattern != null && args != null) {
                int replaceArgs = 0;
                for (int i = 0; i < args.length(); ++i) {
                    switch (args.charAt(i)) {
                        case 'g':
                            singleRun = false;
                            break;
                        case 'd':
                            replaceArgs |= Pattern.UNIX_LINES;
                            break;
                        case 'i':
                            replaceArgs |= Pattern.CASE_INSENSITIVE;
                            break;
                        case 'm':
                            replaceArgs |= Pattern.MULTILINE;
                            break;
                        case 's':
                            replaceArgs |= Pattern.DOTALL;
                            break;
                        case 'u':
                            replaceArgs |= Pattern.UNICODE_CASE;
                            break;
                        case 'x':
                            replaceArgs |= Pattern.COMMENTS;
                            break;
                    }
                }

                replace = replace == null ? args : replace;

                Pattern replacePattern = Pattern.compile(pattern, replaceArgs);
                for (Message s : history.get(channel).toArray(new Message[0])) {
                    String text = s.message;
                    String newText = singleRun ? text.replaceFirst(replacePattern.pattern(), replace) : text.replaceAll(replacePattern.pattern(), replace);
                    if (!text.equals(newText)) {
                        rommie.sendMessage(channel, String.format("(%s) <%s> %s", sender, s.sender, newText));

                        Message m = new Message();
                        m.message = newText; m.sender = sender;
                        history.get(channel).add(m);
                        return;
                    }
                }
            }
            return;
        }

        Message m = new Message();
        m.message = message; m.sender = sender;
        history.get(channel).add(m);
    }

}
