package rommie.modules.BeamPro;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import rommie.modules.GoogleResults.Result;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ChannelInfo {

    public static String webPage = "";



    public static String getInfo() {

        HttpURLConnection connection = null;

        try {
            // get URL content
            URL url = new URL("https://beam.pro/api/v1/channels/tlovetech");
            URLConnection conn = url.openConnection();
            //connection.setRequestProperty("User-Agent", "BOT_ROMMIE");

            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));

            String inputLine;

            //use FileWriter to write file
            while ((inputLine = br.readLine()) != null) {
                if(inputLine.contains("url")){
                    webPage = inputLine;
                }
            }

            br.close();

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return webPage;
    }

    public static String webpage() throws ParseException {
        String STREAM_RESULT = "";
        String page = webPage;

        JSONParser jsonParser = new JSONParser();

        JSONObject jsonObject = (JSONObject) jsonParser.parse(page);


        String name = (String) jsonObject.get("name");
        System.out.println("Name - " + name);

        String token = (String) jsonObject.get("token");
        System.out.println("Token - " + token);

        boolean online = (boolean) jsonObject.get("online");
        System.out.println("Online - " + online);

        if(online) {
            STREAM_RESULT = token + " is currently LIVE! He is streaming " + name;
        }
        else{
            STREAM_RESULT = token + " is currently OFFLINE";
        }

        return STREAM_RESULT;
    }

}
