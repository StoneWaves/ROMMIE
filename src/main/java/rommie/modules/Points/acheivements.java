package rommie.modules.Points;

import rommie.Rommie;

public class acheivements {

    static void acheivements(int Index, Rommie instance){

        if(Points.POINTS.get(Index) == 100){
            instance.sendNotice(Points.USER.get(Index), "Congratulations! You hit 100 messages.");
            instance.sendNotice(Points.USER.get(Index), "Only 900 more to the next level!");
        }
        else if(Points.POINTS.get(Index) == 1000){
            instance.sendNotice(Points.USER.get(Index), "Congratulations! You hit 1000 messages.");
            instance.sendNotice(Points.USER.get(Index), "Only 4,000 more to the next level!");
        }
        else if(Points.POINTS.get(Index) == 5000){
            instance.sendNotice(Points.USER.get(Index), "Congratulations! You hit 5000 messages.");
            instance.sendNotice(Points.USER.get(Index), "Only 5,000 more to the next level!");
        }
        else if(Points.POINTS.get(Index) == 10000){
            instance.sendNotice(Points.USER.get(Index), "Congratulations! You hit 10000 messages.");
            instance.sendNotice(Points.USER.get(Index), "Only 40,000 more to the next level!");
        }
        else if(Points.POINTS.get(Index) == 50000){
            instance.sendNotice(Points.USER.get(Index), "Congratulations! You hit 50000 messages.");
            instance.sendNotice(Points.USER.get(Index), "Thats all the levels for now!");
        }
    }
}
