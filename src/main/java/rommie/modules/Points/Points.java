package rommie.modules.Points;

import rommie.Rommie;

import java.io.*;
import java.util.ArrayList;

public class Points {

    static String FILE_NAME = "Rommie.points";
    static File points = new File(FILE_NAME);

    static ArrayList<String> USER = new ArrayList<>();
    static ArrayList<Integer> POINTS = new ArrayList<>();

    static int USER_LOCATION;
    static boolean USER_FOUND = false;

    // Main points method
    public static void runPoints(String user, Rommie instance) throws IOException {
        // Make the file if it's not found
        if (!points.exists()) points.createNewFile();

        // Find the user in the array
        findUser(user, instance);

        // Saves new data to file
        save();
    }

    // Update the array list from the file
    public static void update() throws IOException {

        USER.clear();
        POINTS.clear();


        BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME));
        String line = reader.readLine();
        while (line != null) {
            String[] split = line.split(":");
            for(int i = 0; i < split.length; i++){
                //System.out.println(split[i]);
                USER.add(split[i]);
                i++;
                POINTS.add(Integer.parseInt(split[i]));
            }


            line = reader.readLine();
        }
    }

    // Find a user in the array list
    static void findUser(String User, Rommie instance) throws FileNotFoundException {

        // Find the user location
        for(int i = 0; i < USER.size(); i++){
            if(USER.get(i).contains(User)){
                // Increase user points
                USER_LOCATION = i;
                USER_FOUND = true;
                break;
            }
        }

        // Handle user depending on if we find them
        if(USER_FOUND){
            increment(USER_LOCATION, instance);
            USER_FOUND = false;
        }
        else{
            newUser(User);
        }
    }

    // Incraese the users' points
    static  void increment(int Index, Rommie instance){
        POINTS.set(Index, POINTS.get(Index) + 1);
        acheivements.acheivements(Index, instance);
    }

    // Add a new user to the array list
    static void newUser(String User){
        USER.add(User);
        POINTS.add(1);
    }

    // Save the array list to the file
    static void save(){
        BufferedWriter myOutFile;
        try {
            // Set up the file writer
            myOutFile = new BufferedWriter( new FileWriter(FILE_NAME, false ));
            for(int i = 0; i < USER.size(); i++){
                // Send out the new data
                myOutFile.write(USER.get(i) + ":" + POINTS.get(i) + "\n");
            }
            myOutFile.close();
        }
        catch( IOException f )
        {
            f.printStackTrace();
        }
    }
}

