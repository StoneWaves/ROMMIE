package rommie.modules.SCCDebug;

import rommie.Rommie;
import rommie.commands.CommandBase;

public class SourceDebug {

    public static void check(String channel, String message, Rommie rommie) {
        if (message.contains("{ROMMIE_CHECK_COMMANDS}")) {
            StringBuilder builder = new StringBuilder("My Commands Are: ");
            for (CommandBase command : rommie.commands) {
                builder.append(command.getCommandName() + ", ");
            }
            rommie.sendMessage(channel, builder.toString());
        } else if (message.contains("{ROMMIE_CHECK_PREFIX}")) {
            rommie.sendMessage(channel, "My prefix is: " + Rommie.CMD_PREFIX);
        }
    }

}
