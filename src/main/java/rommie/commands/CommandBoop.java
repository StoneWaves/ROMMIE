package rommie.commands;

import org.jibble.pircbot.User;

import rommie.Rommie;

/**
 * <h1>Boop a user</h1>
 * The CommandBoop class will boop a suplied target.
 * <p>
 * When no target is supplied everyone will get booped.
 *
 * @author  StoneWaves
 */
public class CommandBoop extends CommandBase {

	public CommandBoop()
	{
		super("boop", 3);
	}
	/**
	 * @param user Unused
	 * @param channel This is the second parameter to addNum method.
	 * @param args This is the array of arguments appended to the command.
	 * @param message This is the full message sent by the user.
	 * @param instance This is an instance of Rommie allowing access to her method.
	 */
	@Override
	public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
	{
		if (args.length < 2) {
			instance.sendAction(channel, "boops everyone");
		}
		else{
			int starting_point = message.indexOf("boop")+"boop ".length();
			String message_to_send = message.substring(starting_point).trim();
            if(message_to_send.equalsIgnoreCase("Rommie")){
                instance.sendMessage(channel, "I shall not boop myself!");
            }
            else {
                instance.sendAction(channel, "boops " + message_to_send);
            }
		}
	}

	/**
	 * This returned string explains the command to the user.
	 * @return String Explains use of the command.
	 */
	@Override
	public String getUsageHelp() {
		return "boop <target>. Supply no target to boop everyone.";
	}

}