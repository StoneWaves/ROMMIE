package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandTogglePrefix extends CommandBase {

    public CommandTogglePrefix() {
        super("toggleprefix", 0);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException {
        if (args.length > 1) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.STATE_PREFIX = !instance.STATE_PREFIX;
            instance.sendMessage(channel, "Stating prefix on join has been toggled to " + instance.STATE_PREFIX);
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Toggle the prefix message on join";
    }
}