package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandTogglePotato extends CommandBase {

    public CommandTogglePotato() {
        super("togglepotato", 0);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException {
        if (args.length > 1) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.POTATO_MESSAGE = !instance.POTATO_MESSAGE;
            instance.sendMessage(channel, "Random potatoes have been toggled to " + instance.POTATO_MESSAGE);
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Toggle the random potatoes.";
    }
}