package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandTribble extends CommandBase {

    public CommandTribble()
    {
        super("tribble", 3);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
    {
        if (args.length < 2) {
            instance.sendAction(channel, "buries everybody in a pile of Tribbles.");
        }
        else{
            int starting_point = message.indexOf("tribble")+"tribble ".length();
            String message_to_send = message.substring(starting_point).trim();
            if(message_to_send.equalsIgnoreCase("Rommie")){
                instance.sendAction(channel, "makes Tribble noises");
            }
            else {
                instance.sendAction(channel, "pelts " + message_to_send + " with Tribbles.");
            }
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Tribble <target>. All the Tribbles!";
    }

}
