package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;
import rommie.modules.SourceCodedWallpapers.Submit;

public class CommandUploadFox extends CommandBase {

    public CommandUploadFox() {
        super("uploadfox", 3);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException {
        if (args.length > 2) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.FOX_MESSAGE = !instance.FOX_MESSAGE;
            instance.sendMessage(channel, Submit.Submit(message, args));
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Upload a fox to the community wallpapers project.";
    }
}