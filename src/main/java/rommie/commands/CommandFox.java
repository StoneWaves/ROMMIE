package rommie.commands;

import org.jibble.pircbot.User;

import rommie.Rommie;

/**
 * <h1>Throw the fox!</h1>
 * Throws a fox at the supplied target.
 * <p>
 * Where no target is supplied everyone will get foxes thrown at them.
 *
 * @author  StoneWaves
 */
public class CommandFox extends CommandBase {

	public CommandFox()
	{
		super("fox", 3);
	}

	/**
	 * @param user Unused
	 * @param channel This is the second parameter to addNum method.
	 * @param args This is the array of arguments appended to the command.
	 * @param message This is the whole message sent by the user.
	 * @param instance This is an instance of Rommie allowing access to her method.
	 */
	@Override
	public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
	{
		if (args.length < 2) {
			instance.sendAction(channel, "throws foxes at everyone");
		}
		else{
			int starting_point = message.indexOf("fox")+"fox ".length();
			String message_to_send = message.substring(starting_point).trim();

            if(message_to_send.contains("Rommie")){
                instance.sendMessage(channel, "I shall not throw a fox at myself!");
            }
            else {
                instance.sendAction(channel, "throws a fox at " + message_to_send);
            }
		}
	}

	/**
	 * This returned string explains the command to the user.
	 * @return String Explains use of the command.
	 */
	@Override
	public String getUsageHelp() {
		return "fox <target>. Supply no target to throw foxes at everyone.";
	}

}