package rommie.commands;

import com.google.gson.Gson;
import org.jibble.pircbot.User;
import rommie.Rommie;
import rommie.modules.GoogleResults.GoogleResults;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;

/**
 * <h1>Google all the things</h1>
 * Allows the user to pass a query to Google and receive the first result.
 *
 * @author  StoneWaves
 */
public class CommandGoogle  extends CommandBase {

    public CommandGoogle()
    {
        super("google", 3);
    }

    /**
     * Calls the correct method and sends the responce containing
     * the first result directly to the channel it received the
     * request from.
     *
     * @param user Unused
     * @param channel This is the second parameter to addNum method.
     * @param args This is the array of arguments appended to the command.
     * @param message This is the whole message sent by the user.
     * @param instance This is an instance of Rommie allowing access to her method.
     *
     * @see rommie.modules.GoogleResults
     */
    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws IOException {
        if (args.length < 2) {
            this.sendUsageMessage(channel, instance);
        } else {
            String address = "http://ajax.googleapis.com/ajax/services/search/web?start=0&rsz=small&v=1.0&q=";

            int starting_point = message.indexOf(args[1]);
            String query = message.substring(starting_point);
            String charset = "UTF-8";

            URL url = new URL(address + URLEncoder.encode(query, charset));
            Reader reader = new InputStreamReader(url.openStream(), charset);
            GoogleResults results = new Gson().fromJson(reader, GoogleResults.class);


            if(results.getResponseData().getResults().size() == 0){
                instance.sendMessage(channel, user + " : No results were found for your search of " + query);
            }
            else {
                // Show title and URL of each results
                String ResultTitle = results.getResponseData().getResults().get(0).getTitle();
                String ResultURL = results.getResponseData().getResults().get(0).getUrl();
                String ResultContent = results.getResponseData().getResults().get(0).getContent();

                String ResultOutput = user + " : " + ResultURL + " -- " + ResultTitle + " : " + ResultContent;

                // Remove tags on the returned results


                ResultOutput = ResultOutput.replaceAll("&#39;", "'");
                ResultOutput = ResultOutput.replaceAll("</b>", "");
                ResultOutput = ResultOutput.replaceAll("<b>", "");

                instance.sendMessage(channel, ResultOutput);
            }
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "google <query>. Get the first result for your query.";
    }

}
