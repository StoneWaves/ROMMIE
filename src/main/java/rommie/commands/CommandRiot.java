package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandRiot extends CommandBase{

    public CommandRiot()
    {
        super("riot", 3);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
    {
        if (args.length < 2) {
            instance.sendMessage(channel, "This command is currenty broken");
        }
        else{
            int starting_point = message.indexOf("RIOT")+"RIOT ".length();
            String message_to_send = message.substring(starting_point).trim();
            instance.sendMessage(channel, "this command is currenty broken");
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "riot <what to riot about>. Supply no target to start a mass riot.";
    }
}
