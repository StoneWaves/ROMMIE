package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

/**
 * <h1>Flail away!</h1>
 * Flails at supplied target.
 * <p>
 * Where no target is supplied everyone will get flailed at.
 *
 * @author  StoneWaves
 */

public class CommandFlail extends CommandBase {

    public CommandFlail()
    {
        super("flail", 3);
    }

    /**
     * @param user Unused
     * @param channel This is the second parameter to addNum method.
     * @param args This is the array of arguments appended to the command.
     * @param message This is the whole message sent by the user.
     * @param instance This is an instance of Rommie allowing access to her method.
     */
    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
    {
        if (args.length < 2) {
            instance.sendAction(channel, "flails at everyone");
        }
        else{
            int starting_point = message.indexOf("flail")+"flail ".length();
            String message_to_send = message.substring(starting_point).trim();

            if(message_to_send.equalsIgnoreCase("Rommie")){
                instance.sendMessage(channel, "I shall not flail at myself!");
            }
            else {
                instance.sendAction(channel, "flails at " + message_to_send);
            }
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "flail <target>. Supply no target to flail at everyone.";
    }

}