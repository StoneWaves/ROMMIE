package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandToggleFox extends CommandBase {

    public CommandToggleFox() {
        super("togglefox", 0);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException {
        if (args.length > 1) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.FOX_MESSAGE = !instance.FOX_MESSAGE;
            instance.sendMessage(channel, "Random foxes have been toggled to " + instance.FOX_MESSAGE);
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Toggle the random foxes.";
    }
}