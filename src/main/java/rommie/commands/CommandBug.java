package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;
/**
 * <h1>Used to report a bug</h1>
 * Sends a user a link to the new issue page for Rommie
 *
 * @author  StoneWaves
 */
public class CommandBug extends CommandBase {

    public CommandBug()
    {
        super("bug", 3);
    }

    /**
     * @param user Unused
     * @param channel This is the second parameter to addNum method.
     * @param args This is the array of arguments appended to the command.
     * @param message Unused
     * @param instance This is an instance of Rommie allowing access to her method.
     */
    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
    {
        if (args.length > 1) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.sendMessage(channel, "Got a problem? Report it here.. https://github.com/StoneWaves/ROMMIE/issues/new");
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Bug. Provides the link to Rommie's issue page";
    }
}