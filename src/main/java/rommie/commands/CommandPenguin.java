package rommie.commands;

import org.jibble.pircbot.User;

import rommie.Rommie;

public class CommandPenguin extends CommandBase {

	public CommandPenguin()
	{
		super("penguin", 3);
	}

	@Override
	public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
	{
		if (args.length < 2) {
			instance.sendAction(channel, "starts the penguin invasion!");
		}
		else{
			int starting_point = message.indexOf("penguin")+"penguin ".length();
			String message_to_send = message.substring(starting_point).trim();
            if(message_to_send.equalsIgnoreCase("Rommie")){
                instance.sendMessage(channel, "Can I even be my own slave?");
                instance.sendAction(channel, "sets her penguin army on " + user);
            }
            else {
                instance.sendAction(channel, "turns " + message_to_send + " into her penguin slave.");
            }
		}
	}

	/**
	 * This returned string explains the command to the user.
	 * @return String Explains use of the command.
	 */
	@Override
	public String getUsageHelp() {
		return "penguin <target>. Without a target the penguins shall kill you all.";
	}

}
