package rommie.commands;

import org.jibble.pircbot.User;
import org.json.simple.parser.ParseException;
import rommie.Rommie;
import rommie.modules.BeamPro.ChannelInfo;

/**
 * <h1>Check the status of a beam.pro stream</h1>
 * Checks the status of a Beam.pre stream
 * <p>
 * In it's current state the only stream that can be checked is that of
 * Tlovetech (user 3181)
 * <b>Note:</b> This command feature is still in development.
 *
 * @author  StoneWaves
 */

public class CommandBeamStream extends CommandBase {

    public CommandBeamStream()
    {
        super("beam", 3);
    }

    /**
     * @param user Unused
     * @param channel This is the second parameter to addNum method.
     * @param args This is the array of arguments appended to the command.
     * @param message Unused
     * @param instance This is an instance of Rommie allowing access to her method.
     */
    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance)
    {
        if (args.length > 2) {
            this.sendUsageMessage(channel, instance);
        }
        else{
            ChannelInfo.getInfo();
            try {
                instance.sendMessage(channel, ChannelInfo.webpage());
            } catch (ParseException e) {
                e.printStackTrace();
                instance.sendMessage(channel, e.getMessage());
            }
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "beamstream. Returns the status of Tlovetech's beam stream.";
    }

}
