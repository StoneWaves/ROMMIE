package rommie.commands;

import org.jibble.pircbot.User;
import rommie.Rommie;

public class CommandToggleGreeting extends CommandBase {

    public CommandToggleGreeting() {
        super("togglegreeting", 0);
    }

    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException {
        if (args.length > 1) {
            this.sendUsageMessage(channel, instance);
        } else {
            instance.GREETING = !instance.GREETING;
            instance.sendMessage(channel, "Greetings have been toggled to " + instance.GREETING);
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "Toggle the greeting message on join";
    }
}