package rommie.commands;

import org.jibble.pircbot.User;

import org.json.simple.parser.ParseException;
import rommie.Rommie;
import rommie.modules.SourceCodedWallpapers.GetWallpaper;

/**
 * <h1>All the foxes</h1>
 * Get a random fox image from the community wallpaper project (http://sourcecoded.info/wallpaper/).
 *
 * @author  StoneWaves
 */
public class CommandFoxImage extends CommandBase {

    public CommandFoxImage()
    {
        super("foximage", 3);
    }

    /**
     * @param user Unused
     * @param channel This is the second parameter to addNum method.
     * @param args This is the array of arguments appended to the command.
     * @param message Unused
     * @param instance This is an instance of Rommie allowing access to her method.
     */
    @Override
    public void performCommand(User user, String channel, String[] args, String message, Rommie instance) throws InterruptedException, ParseException {
        if (args.length > 2) {
            this.sendUsageMessage(channel, instance);
        }
        else{
            GetWallpaper.getWallpaper();
            instance.sendMessage(channel, GetWallpaper.webpage());
        }
    }

    /**
     * This returned string explains the command to the user.
     * @return String Explains use of the command.
     */
    @Override
    public String getUsageHelp() {
        return "foximage. Sends a random fox image.";
    }

}