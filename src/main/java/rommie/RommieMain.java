package rommie;

import org.jibble.pircbot.NickAlreadyInUseException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

class RommieMain {

    public static final Properties config = new Properties();

    public static void main(String[] args) throws Exception {

        //Load the config file
        try {
            config.load(new FileInputStream("Rommie.properties"));
        } catch (IOException ioex) {
            ioex.printStackTrace();
            System.err.println("Error loading config file: Rommie.properties");
            System.exit(0);
        }

        // Now start our bot up.
        Rommie bot = new Rommie();

        //Set nick change to true
        bot.setAutoNickChange(true);

        // Enable debugging output.
        bot.setVerbose(false);

        // Connect to the IRC server.
        try{
            bot.connect(config.getProperty("SERVER"), Integer.parseInt(config.getProperty("PORT")), config.getProperty("SERVER_PASSWORD"));
            //bot.connect("irc.esper.net", 5555);
        }
        catch (NickAlreadyInUseException nickInUse){
            System.out.println(nickInUse.getMessage());
        }

        //identify with nickserv
        bot.identify(config.getProperty("ident"));

        // Join channels.
        standardChannels(bot);
        //devChannels(bot);

    }

    private static void standardChannels(Rommie bot){
        ArrayList<String> CHANNELS = new ArrayList<>(Arrays.asList(RommieMain.config.get("CHANNELS").toString().split(",")));

        for (int i = 0; i < CHANNELS.size(); i++){
            bot.joinChannel(CHANNELS.get(i));
        }
    }

    private static void devChannels(Rommie bot){
        ArrayList<String> CHANNELS = new ArrayList<>(Arrays.asList(RommieMain.config.get("DEVCHANNELS").toString().split(",")));

        for (int i = 0; i < CHANNELS.size(); i++){
            bot.joinChannel(CHANNELS.get(i));
        }
    }
}
