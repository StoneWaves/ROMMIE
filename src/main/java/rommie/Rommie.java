package rommie;

import org.jibble.pircbot.Colors;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;
import org.json.simple.parser.ParseException;
import rommie.commands.*;
import rommie.modules.CustomMessages.greetings;
import rommie.modules.CustomMessages.responces;
import rommie.modules.HighFive.HighFives;
import rommie.modules.Points.Points;
import rommie.modules.Regex.Links;
import rommie.modules.Regex.RegexReplace;
import rommie.modules.SCCDebug.SourceDebug;
import rommie.modules.SourceCodedWallpapers.GetWallpaper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static rommie.modules.RandomNumber.RandomNumber.generateRandom;

public class Rommie extends PircBot {

    //Load in properties from config file
    private final String TIMEOUT_DIR = RommieMain.config.getProperty("TIMEOUT_DIR");
    private final String TIMEOUT_FILE = RommieMain.config.getProperty("TIMEOUT_FILE");
    private final String BOT_NAME = RommieMain.config.getProperty("BOT_NAME");
    private final String POTATO = RommieMain.config.getProperty("DISCONNECT_PASSWORD");
    private static ArrayList<String> CREATOR = new ArrayList<>(Arrays.asList(RommieMain.config.get("CREATOR").toString().split(",")));
    public static String CMD_PREFIX = RommieMain.config.getProperty("CMD_PREFIX");
    public static final String DATA_PATH = RommieMain.config.getProperty("DATA_PATH");
    //TODO Add new admins here
    private String MESSAGE_CHANNEL_STONEWAVES = RommieMain.config.getProperty("MESSAGE_CHANNEL");
    private String MESSAGE_CHANNEL_BLUSUNRISE = RommieMain.config.getProperty("MESSAGE_CHANNEL");
    private String MESSAGE_CHANNEL_CANDICE = RommieMain.config.getProperty("MESSAGE_CHANNEL");
    private String MESSAGE_CHANNEL_SOURCE = RommieMain.config.getProperty("MESSAGE_CHANNEL");
    private String MESSAGE_CHANNEL_K4 = RommieMain.config.getProperty("MESSAGE_CHANNEL");


    private final DateFormat dateFormatTime = new SimpleDateFormat("HH:mm:ss");
    private final Date DATE = new Date();
    private final String ROMMIE_CHANNEL = "#Rommie";
    private final String OVERLOARD = "StoneWaves";


    private final HashMap<String, User[]> channelUserList = new HashMap<>();



    //Config variables
    public boolean STATE_PREFIX = false;
    public boolean GREETING = false;
    public boolean FOX_MESSAGE = true;
    public boolean POTATO_MESSAGE = true;
    String account = "";

    private static ArrayList<String> REJOIN_CHANNELS = new ArrayList<>();

    public String[] Moxie = {"o/", "*rubs MoxieGrrl with a tortoise.*", "*runs away from MoxieGrrl*"};

    //Task for the random foxes
    public TimerTask tt = new TimerTask()
    {
        public void run()
        {
            try {
                sendMessage("#kihira", "Random fox time!");
                GetWallpaper.getWallpaper();
                sendMessage("#kihira", GetWallpaper.webpage());
            } catch (ParseException | InterruptedException e) {
                e.printStackTrace();
                sendMessage("#Kihira", "Ummm, something didn't go to plan there...");
            }
            //Set timer to 24 hours
            new Timer().schedule(tt, generateRandom(1440) * 60000, generateRandom(1440) * 60000);
        }
    };



    //Saves the currently pending command requests, to be handled next tick.
    public List<CommandRequest> requestedCommands = new ArrayList<CommandRequest>();
    //The list of commands Rommie can perform
    public List<CommandBase> commands = new ArrayList<CommandBase>();


    //Main Rommie method
    public Rommie() throws IOException {
        //Set our name
        this.setName(BOT_NAME);

        //Load the commands
        this.setupCommands();

        // Update the points arrays from the file
        Points.update();

        // Run the timer (<timer task>, <sec>*60000, <sec>*60000)
        new Timer().schedule(tt, generateRandom(1440) * 60000, generateRandom(1440) * 60000);

        final Thread thread = new Thread() {
            Scanner scanner = new Scanner(System.in);
            public void run() {
                Thread.currentThread().setName("Rommie|CLI for build");
                try {
                    while (Thread.currentThread().isAlive()) {
                        if(scanner.nextLine().equalsIgnoreCase("kill")){
                            disconnect();
                            System.exit(0);
                        }
                    }
                } catch (Exception e) {
                    sendMessage(OVERLOARD, e.getMessage());
                }
            };
        };
        thread.start();

        sendMessage(ROMMIE_CHANNEL, "Successfully started.");
    }

    //Load in commands array
    public void setupCommands(){

        this.commands.clear();

        //TODO Add any new commands to the list to register them
        //TODO Also update the README.md
        commands.add(new CommandBeamStream());
        commands.add(new CommandBoop());
        commands.add(new CommandBug());
        commands.add(new CommandFlail());
        commands.add(new CommandFox());
        commands.add(new CommandFoxImage());
        commands.add(new CommandGoogle());
        commands.add(new CommandHelp());
        commands.add(new CommandHug());
        commands.add(new CommandJoin());
        commands.add(new CommandPart());
        commands.add(new CommandPenguin());
        commands.add(new CommandPrefix());
        commands.add(new CommandQuote());
        commands.add(new CommandRiot());
        commands.add(new CommandSetPrefix());
        commands.add(new CommandSetTopic());
        commands.add(new CommandSource());
        commands.add(new CommandTable());
        commands.add(new CommandTimeout());
        commands.add(new CommandTimeoutLink());
        commands.add(new CommandToggleFox());
        commands.add(new CommandToggleGreeting());
        commands.add(new CommandTogglePotato());
        commands.add(new CommandTogglePrefix());
        commands.add(new CommandTribble());
        commands.add(new CommandUnTable());
        commands.add(new CommandUploadFox());

/*
        File commandDir = new File("src/main/java/rommie/commands");
        if(commandDir.exists()) {



            for (String listed : commandDir.list())
                if (listed.endsWith(".java")) {



                    String className = listed.substring(0, listed.lastIndexOf(".java"));
                    try {

                        Class<?> commandClass = Class.forName("rommie.commands." + className);

                        boolean hasEmptyConstructor = false;
                        for (Constructor<?> constructor : commandClass.getConstructors())
                            if (constructor.getParameterTypes().length < 1)
                                hasEmptyConstructor = true;

                        if (commandClass != null && !Modifier.isAbstract(commandClass.getModifiers()) && CommandBase.class.isAssignableFrom(commandClass) && hasEmptyConstructor) {
                            this.commands.add((CommandBase) commandClass.newInstance());

                        }

                    }
                    catch (Exception e) {
                        System.out.println("Caught exception loading command " + className);
                        System.out.println(e.getStackTrace());
                        for (int i = 0; i > CREATOR.size(); i++)
                            sendMessage(CREATOR.get(i), "Caught exception loading command " + className);
                    }
                }
        }
        */
    }

    //What happens when someone sends a message in a channel
    public void onMessage(String channel, String sender, String login, String hostname, String message) {

        Links.pullLinks(message, this);
        
        //--------------------------------------------------------------------------------------------------------------
         
         if(sender.equalsIgnoreCase("StoneWaves") && message.equalsIgnoreCase(">morning")) {
            sendMessage(channel, "Good morning darlings!");
        }

        //--------------------------------------------------------------------------------------------------------------

        //Goes and sees if we have a command that matches the message structure
        try {
            commandCheck(channel, sender, message);
        } catch (IOException | InterruptedException | ParseException e) {
            e.printStackTrace();
            sendMessage(ROMMIE_CHANNEL, e.getMessage());
        }

        //--------------------------------------------------------------------------------------------------------------

        //Random fox messages
        try {
            responces.generalMessage(channel, sender, message, this);
        } catch (ParseException | InterruptedException e) {
            e.printStackTrace();
            sendMessage(ROMMIE_CHANNEL, e.getMessage());
        }

        //--------------------------------------------------------------------------------------------------------------

        if (channel.equalsIgnoreCase(MESSAGE_CHANNEL_STONEWAVES)) {
            sendMessage(OVERLOARD, dateFormatTime.format(DATE) + " " + channel + " <" + sender + "> " + message + "\n");
        }

        //TODO Find somewhere better for this to live
        //HighFives.doIt(message, login, sender, channel, this);
        RegexReplace.tryReplace(channel, message, sender, this);
        SourceDebug.check(channel, message, this);

/*
        try {
            Points.runPoints(sender, this);
        } catch (IOException e) {
            e.printStackTrace();
        } */
    }

    //Generates a user list and stores it in a hash map
    @Override
    protected void onUserList(String channel, User[] users) {
        channelUserList.put(channel, users);
    }

    //All commands are declared and run from here
    //Called from onMessage
    void commandCheck(String channel, String sender, String message) throws IOException, InterruptedException, ParseException {
        if (message.startsWith(CMD_PREFIX)) {
            message = message.substring(CMD_PREFIX.length()); //Strips command prefix
            String[] arguments = message.split(" ");
            String command = null;

            if (arguments.length > 0 && arguments[0].length() > 0) {
                command = arguments[0].toLowerCase().trim();
            }

            //----------------------------------------------------------------------------------------------------------

            if (command == null) {
                return;
            }
            for(CommandBase cc : this.commands)
                if(cc.matches(command)) {
                    User user = null;
                    for(User u : this.getUsers(channel))
                        if(u.getNick().equalsIgnoreCase(sender))
                            user = u;
                    CommandRequest request = new CommandRequest(cc, user, channel, message);
                    int level = cc.getCommandLevel();

                    //Do a nickserv check on the user account
                    this.sendMessage("NickServ", "info " + sender);
                    if(account.equalsIgnoreCase("")){
                        int NICKSERV_TIMEOUT = 0;
                        while(account.equalsIgnoreCase("") && NICKSERV_TIMEOUT < 20){
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println("Waiting for nickserv response");
                            NICKSERV_TIMEOUT++;
                        }
                        if(level==0){ // USer is Creator
                            request.status = (CREATOR.contains(account)) ? CommandRequest.RequestStatus.APPROVED : CommandRequest.RequestStatus.DENIED;
                            account = "";
                        }
                    }
                    else if(level==1) //User is Op, Voice or Creator
                        request.status = (user.isOp()||CREATOR.contains(account)) ? CommandRequest.RequestStatus.APPROVED : CommandRequest.RequestStatus.DENIED;
                    else if(level==2)// User is Voice or Creator
                        request.status = (user.isOp()||user.hasVoice()||CREATOR.contains(account)) ? CommandRequest.RequestStatus.APPROVED : CommandRequest.RequestStatus.DENIED;
                    else if(level>=3)
                        request.status = CommandRequest.RequestStatus.APPROVED;
                    this.requestedCommands.add(request);
                }
            performPendingCommands();
        }//This brace closes the cmd loop
    }

    void performPendingCommands() throws InterruptedException, IOException, ParseException {

        Iterator<CommandRequest> iterator = this.requestedCommands.iterator();
        while(iterator.hasNext()){
            CommandRequest request = iterator.next();

            switch(request.status){
                case APPROVED:
                    String[] arguments = request.message.split(" ");
                    request.command.performCommand(request.user, request.channel, arguments, request.message, this);
                    iterator.remove();
                    break;
                case DENIED:
                    this.sendMessage(request.channel, request.user.getNick()+", you have no permission to use this command");
                    iterator.remove();
                    break;
                case PENDING:
                default:
                    // Do Nothing because the request is still pending, Derp!
                    break;
            }
        }
    }

    //Used to check if a user exists when NickServ is polled
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice){
        //Tells me if a user does not exist with NickServ
        String CHECK_CHANNEL = "";
        String CHECK_USER = "";

        String unformattedMessage = Colors.removeFormattingAndColors(notice);
        String name = "";
        //Made Global
        // String account = "";

        if(notice.contains("is not registered.") && sourceNick.equalsIgnoreCase("NickServ")){
            sendMessage(CHECK_CHANNEL, "User is not registered.");
            name = unformattedMessage.substring(0, unformattedMessage.indexOf(" is not registered."));
        }
        //This tells me if a user exists with NickServ
        else if(notice.contains("Information on") && notice.contains(CHECK_USER)){
            sendMessage(CHECK_CHANNEL, "User is registered.");
            String keyPhrase = " (account ";
            int indx = unformattedMessage.indexOf(keyPhrase);
            if(indx>0){
                name = unformattedMessage.substring("Information on ".length(), indx);
                account = unformattedMessage.substring(indx+keyPhrase.length(), unformattedMessage.length()-2);
            }
        }
        //Tells me if a user is logged in currently with NickServ
        else if(notice.contains("Last seen  : now") && sourceNick.equalsIgnoreCase("NickServ")){
            sendMessage(CHECK_CHANNEL, notice);
        }

        for(CommandRequest request : this.requestedCommands)
            if(request.user.getNick().equalsIgnoreCase(name))
            {
                sendMessage(ROMMIE_CHANNEL, "Preforming auth check...");
                sendMessage(ROMMIE_CHANNEL, "Account: "+account+", Creator: "+CREATOR+", match: "+CREATOR.contains(account));
                System.out.println("Account: "+account+", Creator: "+CREATOR+", match: "+CREATOR.contains(account));
                if(CREATOR.contains(account)){
                    request.status = CommandRequest.RequestStatus.APPROVED;
                }
                else{
                    request.status = CommandRequest.RequestStatus.DENIED;
                }
            }
        try {
            performPendingCommands();
        }
        catch (InterruptedException | IOException | ParseException e) {
            e.printStackTrace();
            sendMessage(ROMMIE_CHANNEL, e.getMessage());
        }
    }

    //Log timeouts to file
    //Called when a timeout is logged
    //THIS IS A HISTORICAL METHOD
    public void saveTimeout(User sender, Date date) {
        BufferedWriter myOutFile;
        try {
            //Set up the file writer
            myOutFile = new BufferedWriter( new FileWriter( TIMEOUT_DIR + TIMEOUT_FILE, true ) );
            myOutFile.write(sender + " logged a new timeout at " + date + "\n" );
            myOutFile.close();
        } catch( IOException f )
        {
            f.printStackTrace();
        }
    }

    //Called when the bot gets a PM
    protected void onPrivateMessage(String sender, String login, String hostname, String message) {

        //Command to quit IRC
        String[] arguments = message.split(" ");

        //--------------------------------------------------------------------------------------------------------------

        this.sendMessage("NickServ", "info " + sender);
        if(account.equalsIgnoreCase("")){
            int NICKSERV_TIMEOUT = 0;
            while(account.equalsIgnoreCase("") && NICKSERV_TIMEOUT < 5){
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Waiting for nickserv response");
                NICKSERV_TIMEOUT++;
            }
        }

        //What if our account is on the CREATOR list
        //TODO On addition of a creator add them here and create a new message channel
        if (CREATOR.contains(account)) {
            //Channel switch
            if (message.startsWith("#") &&  account.equalsIgnoreCase("StoneWaves")) {
                MESSAGE_CHANNEL_STONEWAVES = message;
            }
            else if (message.startsWith("#") &&  account.equalsIgnoreCase("BluSunrize")) {
                MESSAGE_CHANNEL_BLUSUNRISE = message;
            }
            else if (message.startsWith("#") &&  account.equalsIgnoreCase("Candice")) {
                MESSAGE_CHANNEL_CANDICE = message;
            }
            else if (message.startsWith("#") &&  account.equalsIgnoreCase("SourceCoded")) {
                MESSAGE_CHANNEL_CANDICE = message;
            }
            else if (message.startsWith("#") &&  account.equalsIgnoreCase("K-4U")) {
                MESSAGE_CHANNEL_CANDICE = message;
            }

            //TODO Append when a new admin is added
            //Send messages and receive messages
            if (account.equalsIgnoreCase("StoneWaves")) {
                pm_Relay(MESSAGE_CHANNEL_STONEWAVES, message, arguments);
            }
            else if (account.equalsIgnoreCase("BluSunrize")) {
                pm_Relay(MESSAGE_CHANNEL_BLUSUNRISE, message, arguments);
            }
            else if (account.equalsIgnoreCase("Candice")) {
                pm_Relay(MESSAGE_CHANNEL_CANDICE, message, arguments);
            }
            else if (account.equalsIgnoreCase("SourceCoded")) {
                pm_Relay(MESSAGE_CHANNEL_SOURCE, message, arguments);
            }
            else if (account.equalsIgnoreCase("K-4U")) {
                pm_Relay(MESSAGE_CHANNEL_K4, message, arguments);
            }
        }

        //What if you aren't the creator
        else{
            sendMessage(sender, "I am not authorised to talk to you");
            account = "";
            sendMessage(OVERLOARD, "PM form " + sender + " - " + message);
        }
    }

    //Called from onPrivateMessage
    private void pm_Relay(String MESSAGE_CHANNEL, String message, String[] arguments){
        //Kill command
        if (message.startsWith(CMD_PREFIX + "disconnect") && CREATOR.contains(account) && arguments[1].equalsIgnoreCase(POTATO)) {
            String[] channels = getChannels();
            for (String channel : channels) {
                sendMessage(channel, "Quitting IRC ");
            }
            log("Disconnect command issued");
            disconnect();
            System.exit(0);
        }
        //Send an action
        else if (message.startsWith(".me") && CREATOR.contains(account)) {
            int starting_point = message.indexOf(".me") + ".me ".length();
            String message_to_send = message.substring(starting_point).trim();
            sendAction(MESSAGE_CHANNEL, message_to_send);
        }
        //Dont send channel change
        else if(message.startsWith("#")){}
        //Send anything else
        else {
            sendMessage(MESSAGE_CHANNEL, message);
        }
    }

    //What to do when someone joins a channel (Bot & other users)
    protected void onJoin(String channel, String sender, String login, String hostname) {

        REJOIN_CHANNELS.add(channel);

        //--------------------------------------------------------------------------------------------------------------

        //State prefix when we join if true
        if(sender.equals(getNick()) && STATE_PREFIX) {
            sendMessage(channel, "The current command prefix is " +  CMD_PREFIX);
        }

        //--------------------------------------------------------------------------------------------------------------

        //Send greeting when someone joins if true
        if(!sender.equals(getNick()) && GREETING) {
            sendMessage(channel, "o/ " + sender);
        }

        //--------------------------------------------------------------------------------------------------------------

        //Check for personalised greetings
        greetings.check(this, sender, channel);
    }

    //Go mad with power on OP
    protected void onOp(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient){
        if(recipient.equals(getNick())){
            sendAction(channel, "goes mad with power");
        }
    }

    //Join a channel on invite
    protected void onInvite(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel){
        joinChannel(channel);
        sendMessage(OVERLOARD, "I was invited to join " + channel);
    }

    //What is done when we get kicked
    protected void onKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason){

        if(recipientNick.equalsIgnoreCase(getNick())) {
            sendMessage(OVERLOARD, "I was kicked by " + kickerNick + " from " + channel + " for: " + Colors.PURPLE + reason);
            joinChannel(channel);
        }
    }

    //Try to reconnect when we disconnect
    protected void onDisconnect(){
        while (!isConnected()) {
            try {
                reconnect();
                if(isConnected()){
                    joinChannel(ROMMIE_CHANNEL);
                    for(int i =0; i < REJOIN_CHANNELS.size(); i++)
                    {
                        //Using a bouncer now sothis isnt needed
                        //joinChannel(REJOIN_CHANNELS.get(i));
                    }
                }
            }
            catch (Exception e) {
                System.out.println("Couldn't reconnect");
                System.out.println(e.getMessage());
            }
        }
    }
}
